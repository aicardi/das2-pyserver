"""Default handler for 2.2 style catalog level lists"""

import sys
import codecs
import os
import json

from os.path import join as pjoin
from os.path import dirname as dname
from os.path import basename as bname

##############################################################################
# How URI's cascade
#
# Data catalog
#
#  1. If my _dirinfo_.dsdf has a uri = tag then this catalog has 
#     exactly that URI.
#
#  2. If not read up the stack until hitting a _dirinfo_.dsdf 
#     with a URI.  My uri is the relative path from the nearest
#     one set.
#
#  3. If no _dirinfo_.dsdf have a uri then my uri is the site
#     uri from the config file with my relative path appended.
#
#  4. If the site uri is missing, issue an error message and
#     quit.
def getDirUri(U, fLog, dConf, dDirInfo, sCatDir):
	if "uri" in dDirInfo:
		sUri = dDirInfo["uri"].strip("\"' /r/n")
		fLog.write("INFO: Using exlicit URI for directory %s, %s"%(sCatDir, sUri))
		return sUri
	
	sRelPath = None
	_sOrigCatDir = sCatDir
	while sCatDir != dConf['DSDF_ROOT']:
	
		# Go up one
		if sRelPath == None:
			sRelPath = bname(sCatDir)
		else:
			sRelPath = "%s/%s"%(bname(sCatDir), sRelPath)
		sCatDir = dname(sCatDir)

		sCatDsdf = pjoin(sCatDir, '_dirinfo_.dsdf')
		
		if os.path.isfile(sCatDsdf):
			fIn = codecs.open(sCatDsdf, "rb", encoding='utf-8')
			dCatDsdf = U.dsdf.readDsdf(fIn, fLog)
			fIn.close()
			if "uri" in dCatDsdf:
				fLog.write("INFO:  Directory %s URI set relative to directory %s URI"%(
				           _sOrigCatDir, sCatDir))
				sUri = dCatDsdf["uri"].strip("\"' /r/n")
				return "%s/%s"%( sUri, sRelPath)
	
	# Still here huh, okay
	if "SITE_PATHURI" not in dConf:
		U.webio.serverError(fLog, 
			"No pathUri setting along the path of _dirinfo_.dsdf files leading "+\
		   "the path to file %s and fall back value SITE_PATHURI not set in %s"%(
							  pjoin(_sOrigCatDir, '_dirinfo_.dsdf'), dConf['__file__']))
		return None
	
	fLog.write("INFO:  Directory %s URI set relative to config file SITE_PATHURI: %s"%(
	           _sOrigCatDir, dConf['SITE_PATHURI']))

	return "%s/%s"%(dConf['SITE_PATHURI'], sRelPath)
	
##############################################################################
def getLocations(dDsdf):

	lKeys = sorted(dDsdf.keys())
	lLocs = []
	for sKey in lKeys:
		if sKey.startswith("location"):
			lLocs.append( dDirInfo[sKey].strip("\"' \t\r\n" ) )
	return lLocs

	
##############################################################################
def getCatBody(U, fLog, dConf, sParentUri, sParentUrl, sCatDir):

	lRawItems = os.listdir(sCatDir)
	
	dBody = {}
	
	for sItem in lRawItems:
	
		# I've already read my _dirinfo_.dsdf
		if sItem == '_dirinfo_.dsdf':
			continue
	
		sItemPath = pjoin(sCatDir, sItem)
		
		if os.path.isfile(sItemPath) and sItem.endswith(".dsdf"):
			sItem = sItem.replace('.dsdf','')
			
			fLog.write("INFO:  Reading dsdf %s"%sItemPath)
			
			# Getting Data source URI's
			#
			# 1. If the datasource or catalog has a uri = tag, then it has
			#    exactly that URI
			#
			# 2. If not then the datasource has it's relative path from
			#    this catalog's URI appended to the end.
			dItem = {'type':'Collection'}
			fIn = codecs.open(sItemPath, 'rb', encoding='utf-8')
			dDsdf = U.dsdf.readDsdf(fIn, fLog)
			fIn.close()
			
			if 'description' in dDsdf:
				dItem['title'] = dDsdf['description'].strip("\"' \r\n")
			if 'pathUri' in dDsdf:
				dItem['path_uri'] = dDsdf['uri'].strip("\"' \r\n")
			else:
				dItem['path_uri'] = "%s/%s"%(sParentUri, sItem)
			
			# Data source descriptions are handled differently from
			# directory descriptions.  We know the datasource is here 
			# because there is a dsdf.  It's location parameters are for
			# the actual data read
			dItem['url'] = [ "%s/%s.json"%(sParentUrl, sItem) ]
			dItem['name'] = sItem
			
			dBody[sItem] = dItem
			
		elif os.path.isdir(sItemPath):
			sDsdfPath = pjoin(sItemPath, '_dirinfo_.dsdf')
			if not os.path.isfile(sDsdfPath):
				fLog.write("INFO: Skipping directory %s, _dirinfo_.dsdf missing"%sDsdfPath)
				continue
				
			# Hack to skip low-case directories that have any alphabetic
			# characters in them
			if "SKIP_LOWCASE" in dConf and dConf['SKIP_LOWCASE'] in ('1','yes','true'):
				if sItem.lower() == sItem:
					bHasAlpha = False
					for c in sItem:
						if c.isalpha():
							bHasAlpha = True
					if bHasAlpha:
						continue
				
				
			dItem = {'type':'Catalog'}
			fIn = codecs.open(sDsdfPath, 'rb', encoding='utf-8')
			dDsdf = U.dsdf.readDsdf(fIn, fLog)
			fIn.close()
			if 'description' in dDsdf:
				dItem['title'] = dDsdf['description'].strip("\"' \r\n")
			if 'pathUri' in dDsdf:
				dItem['path_uri'] = dDsdf['uri'].strip("\"' \r\n")
			else:
				dItem['path_uri'] = "%s/%s"%(sParentUri, sItem)

			lLocs = getLocations(dDsdf)
			if len(lLocs) > 0:
				dItem['urls'] = lLocs
			else:
				dItem['urls'] = [ "%s/%s/index.json"%(sParentUrl, sItem) ]
				
			dItem['name'] = sItem
			dBody[sItem] = dItem
		
		else:
			pass  # Just some random junk, move along
				
	return dBody


##############################################################################
def sendDict(fLog, dDict):
	uCat = json.dumps(dDict, ensure_ascii=False,
		               sort_keys=True, indent=3)
	
	sys.stdout.write("Status: 200 OK\r\n")
	sys.stdout.write("Content-Type: application/json; charset=utf-8\r\n\r\n")
	sys.stdout.write(uCat)
	
##############################################################################
def jsonDirectory(U, sCatPath, dConf, fLog):
	sPath = pjoin(dConf['DSDF_ROOT'], sCatPath)
	
	sDirInfo = pjoin(sPath, '_dirinfo_.dsdf')
	if not os.path.isfile(sDirInfo):
		# _dirinfo_.dsdf files are now required
		U.webio.serverError(fLog, """Catalog directory file %s missing"""%sDirInfo)
		return 17
	fIn = codecs.open(sDirInfo, 'rb', encoding='utf-8')
	dDirInfo = U.dsdf.readDsdf(fIn, fLog)
	fIn.close()

	sUri = getDirUri(U, fLog, dConf, dDirInfo, sPath)

	if sUri == None:
		return 17
	
	dCat = {"type":"Catalog", "path_uri":sUri, "name":bname(sPath)}
	
	if "description" in dDirInfo:
		dCat['title'] = dDirInfo['description'].strip("\"' \t\r\n")
		
	# See if this is a remote catalog, if so just give the links and
	# move on.
	lLocs = getLocations(dDirInfo)

	if len(lLocs) > 0:
		dCat['locations'] = lLocs
		sendDict(fLog, dCat)
		return 0
	
	# We are not a remote catalog, find all sub items of this directory
	if len(sCatPath) > 0:
		sMyUrl = pjoin( U.webio.getScriptUrl(), "json", sCatPath)
	else:
		sMyUrl = pjoin( U.webio.getScriptUrl(), "json")
	dBody = getCatBody(U, fLog, dConf, sUri, sMyUrl, sPath)
	if dBody == None:
		return 17
	
	dCat['catalog'] = dBody
	return dCat

##############################################################################
def jsonData(U, sCatPath, dConf, fLog, form):
	dsdf = U.dsdf.Dsdf(sCatPath, dConf, form, fLog)
	sRootUrl = "%s/json/"%U.webio.getScriptUrl()
	dDef = dsdf.getInterfaceDef(dConf, fLog, dConf['SITE_PATHURI'], sRootUrl, False)

	dRes = {'title': dDef['TITLE'], 'type': 'Collection', 'name': bname(dsdf.sName)}
	lContacts = []
	for contact in dDef['SOURCE']['CONTACTS']:
		if dDef['SOURCE']['CONTACTS'][contact]['PURPOSE'] == 'scientific':
			lContacts.append({'name':contact, 
				'email': dDef['SOURCE']['CONTACTS'][contact]['EMAIL']})
	dRes['sci_contacts'] = lContacts
	time = dDef['SOURCE']['COORDINATES']['time']
	dRes['coordinates'] = {'time':{'name': 'Time', 'valid_min': time['RANGE']['MIN'],
					'valid_max': time['RANGE']['MAX']}}
	dRes['sources'] = {'das2': {'convention': 'das2/2.2', 'name': 'Das2/2.2 Source',
					'type': 'HttpStreamSrc', 'purpose': 'primary-stream'}}
	dRes['sources']['das2']['urls'] = [ sRootUrl+sCatPath+'/das2.json' ]

	dEPNcore = {}
	if 'dataproductType' in dsdf.d:
		dEPNcore['dataproduct_type'] = dsdf.d['dataproductType']
	if 'featureName' in dsdf.d:
		dEPNcore['feature_name'] = dsdf.d['featureName']
	if 'measurementType' in dsdf.d:
		dEPNcore['measurement_type'] = dsdf.d['measurementType']
	if 'targetClass' in dsdf.d:
		dEPNcore['target_class'] = dsdf.d['targetClass']
	if 'targetName' in dsdf.d:
		dEPNcore['target_name'] = dsdf.d['targetName']
	if 'targetRegion' in dsdf.d:
		dEPNcore['target_region'] = dsdf.d['targetRegion']
	dRes['EPNcore'] = dEPNcore
	return dRes

##############################################################################
def jsonDas2Data(U, sCatPath, dConf, fLog, form):
	dsdf = U.dsdf.Dsdf(sCatPath, dConf, form, fLog)
	sRootUrl = "%s/json/"%U.webio.getScriptUrl()
	dDef = dsdf.getInterfaceDef(dConf, fLog, dConf['SITE_PATHURI'], sRootUrl, False)

	# Add in our own options
	if dsdf.isTrue('qstream'):
		sNewMime = 'text/vnd.das2.qstream; charset=utf-8'
	else:
		sNewMime = 'text/vnd.das2.das2stream; charset=urf-8'

	dOutOpts = {}
	dDef['SOURCE']['QUERY_PARAMS']['OUTPUT'] = dOutOpts
	dOutOpts['text'] = {'TITLE':'Convert Binary Values to Text', 'TYPE':'boolean',
		'DEFAULT':False, 'REQIRED':False, 'MIME':sNewMime}
	if not dsdf.isTrue('qstream'):
		dDef['SOURCE']['FORMATS']['AVAILABLE'] = [{'MIME':sNewMime, 'VERSION':'2.2'} ]
	else:
		dDef['SOURCE']['FORMATS']['AVAILABLE'] = [{'MIME':sNewMime} ]

	dRes = {'title': dDef['TITLE'], 'type': 'HttpStreamSrc', 'name': 'Das2/2.2 Source'}
	dRes['uris'] = [dDef['PATH_URI']+'/das2']
	if dsdf.d['das2Stream']:
		dRes['format'] = {'default':{'name':'Das2 Stream', 'mime':'application/vnd.das2.das2stream'}}
	lContacts = []
	for contact in dDef['SOURCE']['CONTACTS']:
		if dDef['SOURCE']['CONTACTS'][contact]['PURPOSE'] == 'technical':
			lContacts.append({'name':contact, 
				'email': dDef['SOURCE']['CONTACTS'][contact]['EMAIL']})
	dRes['tech_contacts'] = lContacts
	dInterfaces = {}
	dInterfaces['options'] = {'das2_text': {'name':'das2 text', 
		'set':{'param':'ascii', 'pval':'true', 'value':True},
		'title':'Convert stream to das2 text (utf-8) format',
		'value':False}}
	dTime = {}
	dCoord = dDef['SOURCE']['COORDINATES']
	range_min = dCoord['time']['RANGE']['MIN']
	range_max = dCoord['time']['RANGE']['MAX']
	dTime['maximum'] = {'set': {'param': 'end_time', 'range': [range_min, range_max], 'required': True},
		'value': range_max}
	dTime['minimum'] = {'set': {'param': 'start_time', 'range': [range_min, range_max], 'required': True},
		'value': range_min}
	dTime['resolution'] = {'set': {'param': 'resolution', 'required': False},
		'units': dDef['SOURCE']['QUERY_PARAMS']['COORD_SUBSET']['time']['res']['UNITS'],
		'value': dDef['SOURCE']['QUERY_EXAMPLES'][0]['http_params']['resolution']}
	dTime['units'] = {'value': dCoord['time']['UNITS']}
	dInterfaces['coordinates'] = {'time':dTime}
	dRes['interface'] = dInterfaces

	dProtocol = {}
	if dDef['SOURCE']['ACCESS'][0]['AUTH_REQUIRED'] == 'no':
		dProtocol['authentication'] = {'required': False}
	else:
		dProtocol['authentication'] = {'required': True, 'realm': dsdf.d['securityRealm'] }
	dProtocol['convention'] = 'das2/2.2'
	base_url = U.webio.getScriptUrl() + '?server=dataset&dataset=' + sCatPath
	dProtocol['base_urls'] = [base_url]
	dHttpParams = {}
	dHttpParams['ascii'] = {'name': 'UTF-8', 'required':False, 
		'title':'Insure stream output is readable as UTF-8 text', 'type': 'boolean'} 
	dHttpParams['start_time'] = {'name': 'Min Time', 'required':True,
		'title':'Minimum time value to stream', 'type': 'isotime'} 
	dHttpParams['end_time'] = {'name': 'Max Time', 'required':True,
		'title':'Maximum time value to stream', 'type': 'isotime'} 
	dHttpParams['resolution'] = {'name': 'Resolution', 'required':False,
		'title':'Maximum resolution between output time points',
		'description':'The server will return data at or better than the given resolution if possible.  Leave un-specified to get data at intrinsic resolution without server side averages',
		'type':'real',
		'units':'s'}
	dProtocol['http_params'] = dHttpParams
	dExamples = {}
	for index, example in enumerate(dDef['SOURCE']['QUERY_EXAMPLES']):
		dExample = {'http_params': example['http_params'], 'url': example['URL']}
		dExample['name'] = 'Example %02d'%index
		dExamples['example_%02d'%index] = dExample
	dProtocol['examples'] = dExamples

	dRes['protocol'] = dProtocol
	return dRes


##############################################################################
def handleReq(U, sReqType, dConf, fLog, form, sPathInfo):
	"""See das2server.defhandlers.intro.py for a decription of this function
	interface
	"""
	pout = sys.stdout.write
	
	if 'DSDF_ROOT' not in dConf:
		U.webio.serverError(fLog, u"DSDF_ROOT not set in %s"%dConf['__file__'])
		return 17
	
	# _dirOut and _fileOut append a list of tuples to lOut
	
	lOut = [] 
	tData = ("%s/"%dConf['DSDF_ROOT'], lOut, fLog)
	
	sCatPath = os.getenv("PATH_INFO")  # Knock off leading '/json'
		
	if sCatPath.startswith('/json/'):
		sCatPath = sCatPath[len('/json/'):]
	
	if sCatPath.endswith("index.json"):  # knock off trailing index.json
		sCatPath = sCatPath.replace("index.json", "")
	
	if sCatPath.endswith('/'):
		sCatPath = sCatPath[:-1]
		dJson = jsonDirectory(U, sCatPath, dConf, fLog)
	elif sCatPath.endswith('/das2.json'):
		sCatPath = sCatPath.replace("/das2.json", "")
		dJson = jsonDas2Data(U, sCatPath, dConf, fLog, form)
	else:
		sCatPath = sCatPath.replace(".json", "")
		dJson = jsonData(U, sCatPath, dConf, fLog, form)
	
	sendDict(fLog, dJson)
	return 0
